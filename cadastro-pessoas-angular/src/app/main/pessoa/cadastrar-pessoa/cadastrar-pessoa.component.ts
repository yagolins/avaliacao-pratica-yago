import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { PessoaEnum } from '../model/pessoa.enum';

@Component({
  selector: 'app-cadastrar-pessoa',
  templateUrl: './cadastrar-pessoa.component.html',
  styleUrls: ['./cadastrar-pessoa.component.css']
})
export class CadastrarPessoaComponent implements OnInit {

  formPessoa: FormGroup;

  listaTipo = PessoaEnum.listaTipo;
  static CPF = PessoaEnum.CPF;
  static CNPJ = PessoaEnum.CNPJ;

  constructor(public formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.buildForm();
  }

  private buildForm(): void {
    this.formPessoa = this.formBuilder.group({
      nome: new FormControl('', Validators.required),
      documento: new FormControl('', Validators.required),
      dtNascimento: new FormControl('', Validators.required),
      nomePai: new FormControl('', []),
      nomeMae: new FormControl('', []),
      tipoPessoa: new FormControl('', Validators.required)
    });
  }

  salvar(){
    console.log(this.formPessoa.invalid);
    console.log(this.formPessoa.value);
  }

  selecionaDocumento(): number{
    if(this.formPessoa.get('tipoPessoa').value == CadastrarPessoaComponent.CPF){
      return CadastrarPessoaComponent.CPF;
    }
    if(this.formPessoa.get('tipoPessoa').value == CadastrarPessoaComponent.CNPJ){
      return CadastrarPessoaComponent.CNPJ;
    }
  }
}
