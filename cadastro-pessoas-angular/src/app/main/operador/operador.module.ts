import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MatRadioModule } from '@angular/material/radio';
import { CadastroOperadorComponent } from './cadastro-operador/cadastro-operador.component';

const routes = [
  {
    path: '',
    component: CadastroOperadorComponent
  }
];

@NgModule({
  declarations: [CadastroOperadorComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatRadioModule,
    RouterModule.forChild(routes),
    RouterModule
  ],
  entryComponents: [CadastroOperadorComponent],
  providers: [
      RouterModule
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class OperadorModule { }
