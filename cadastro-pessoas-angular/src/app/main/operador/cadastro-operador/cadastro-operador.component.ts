import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { OperadorEnum } from '../model/operador.enum';


@Component({
  selector: 'app-cadastro-operador',
  templateUrl: './cadastro-operador.component.html',
  styleUrls: ['./cadastro-operador.component.css']
})
export class CadastroOperadorComponent implements OnInit {

  formOperador: FormGroup;

  listaOp = OperadorEnum.listaOperador;

  constructor(public formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.buildForm();
  }

  private buildForm(): void {
    this.formOperador = this.formBuilder.group({
      nome: new FormControl('', Validators.required),
      login: new FormControl('', Validators.required),
      senha: new FormControl('', Validators.required),
      perfil: new FormControl('', Validators.required)
    });
  }

  salvar(){

  }

}
