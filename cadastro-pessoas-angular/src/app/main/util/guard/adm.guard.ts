import { CanActivate, ActivatedRouteSnapshot, Router } from "@angular/router";
import { Injectable } from "@angular/core";

@Injectable()
export class AdministradorGuard implements CanActivate {
  private identity: any;

  constructor(public router: Router) {}

  canActivate(route: ActivatedRouteSnapshot): boolean {
    const roles: any[] = route.data.roles;
    this.identity = JSON.parse(localStorage.getItem('identity'));
    if (!!this.identity && this.identity.perfil == 3 ) {
      return true;
    }
    this.router.navigate(['inicio']);
  }
}
