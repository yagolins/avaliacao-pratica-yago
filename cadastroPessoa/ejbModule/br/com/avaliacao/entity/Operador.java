package br.com.avaliacao.entity;
 
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
 
@Getter
@Setter
@Entity
@Table(name = "tb_operador")
public class Operador implements Serializable {
 
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
 
    @Column(name = "nome", length = 100, nullable = false)
    private String nome;
 
    @Column(name = "login", length = 50, nullable = false)
    private String login;
    
    @Column(name = "senha", length = 50, nullable = false)
    private String senha;
    
    @Column(name = "dataCadastro", length = 50, nullable = false)
    private Date dataCadastro;
 
}