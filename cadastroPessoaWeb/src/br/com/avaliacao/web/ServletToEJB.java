package br.com.avaliacao.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.avaliacao.bean.OperadorBeanLocal;
import br.com.avaliacao.entity.Operador;

@WebServlet("/ServletToEJB")
public class ServletToEJB extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@EJB
	private OperadorBeanLocal operadorBeanLocal;
	
    public ServletToEJB() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		operadorBeanLocal.listarOperadores();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Operador operador = new Operador();
        operador.setNome("Yago");
        operador.setLogin("yago@gmail.com");
        operador.setSenha("123456");
        operador.setDataCadastro(new Date());
        operadorBeanLocal.adicionarOperador(operador);
		
		System.out.println("funcionou");
	}
	
	@Override
	protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String id = req.getParameter("id");
		Operador operador = operadorBeanLocal.listarOperador(Integer.valueOf(id));
		operadorBeanLocal.excluirOperador(operador);
		super.doDelete(req, resp);
	}
	
	@Override
	protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Operador operador = new Operador();
        operador.setNome("Yago");
        operador.setLogin("yago@gmail.com");
        operador.setSenha("123456");
        operador.setDataCadastro(new Date());
        operadorBeanLocal.editarOperador(operador);
		
		System.out.println("funcionou");
		super.doPut(req, resp);
	}

}
