export class NumericItem {
  nome: string;
  value: number;

  constructor(nome: string, value: number) {
    this.nome = nome;
    this.value = value;
  }

}
