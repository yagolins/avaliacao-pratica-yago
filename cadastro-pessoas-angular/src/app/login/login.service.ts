import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  urlBase = "http://localhost:8080/cadastroPessoaWeb/";

  headers: HttpHeaders;

  constructor(
    private _httpClient: HttpClient
  ) {}

  excluirOperador(id: any, callbackSuccess: any, callbackError: any, callbackFinally: any): any {
    const option = {
      headers: this.headers
    };

    this._httpClient.delete(this.urlBase + 'operador/'+id, option)
      .subscribe(
        (response: any) => {
          callbackSuccess(response);
        },
        error => {
          callbackError(error);
        }
      );
  }

  findAllOperadores(callbackSuccess: any, callbackError: any, callbackFinally: any): any {
    const option = {
      headers: this.headers
    };

    this._httpClient.get(this.urlBase + 'operador/', option)
      .subscribe(
        (response: any) => {
          callbackSuccess(response);
        },
        error => {
          callbackError(error);
        }
      );
  }
}
