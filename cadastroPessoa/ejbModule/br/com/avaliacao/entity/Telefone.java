package br.com.avaliacao.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
//@Entity
//@Table(name = "tb_telefone")
public class Telefone {
	
//	@Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Column(name = "id")
	private Integer id;
	private Long ddd;
	private String numero;
	private Integer tipo;
	private Date dataCadastro;
	private String login;
	
	private Pessoa pessoa;
}
