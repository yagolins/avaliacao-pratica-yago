import { NumericItem } from '../../util/numeric-item.model';

export class OperadorEnum {

  static listaOperador = [
    new NumericItem('Gerente', 1),
    new NumericItem('Analista', 2),
  ];

}
