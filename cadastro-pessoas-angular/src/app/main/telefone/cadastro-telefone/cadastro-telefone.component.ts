import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogConfig, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-cadastro-telefone',
  templateUrl: './cadastro-telefone.component.html',
  styleUrls: ['./cadastro-telefone.component.css']
})
export class CadastroTelefoneComponent implements OnInit {

  constructor(private matDialog: MatDialog,
    private dialogRef: MatDialogRef<CadastroTelefoneComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
  }

  fecharModal() {
    this.dialogRef.close();
  }

  static getConfDialog(path64, dados) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.height = '50em';
    dialogConfig.width = '75em';
    dialogConfig.data = { arquivo64: path64, dados: dados };
    return dialogConfig;
  }

}
