import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MatRadioModule } from '@angular/material/radio';
import { CadastroTelefoneComponent } from './cadastro-telefone/cadastro-telefone.component';

const routes = [
  {
    path: '',
    component: CadastroTelefoneComponent
  }
];

@NgModule({
  declarations: [CadastroTelefoneComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatRadioModule,
    RouterModule.forChild(routes),
    RouterModule
  ],
  entryComponents: [CadastroTelefoneComponent],
  providers: [
      RouterModule
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class TelefoneModule { }
