package br.com.avaliacao.bean;

import java.util.Collection;

import javax.ejb.Remote;

import br.com.avaliacao.entity.Operador;

@Remote
public interface OperadorBeanLocal {
	
	public boolean adicionarOperador(Operador operador);
	
	public Collection<Operador> listarOperadores();
	
	public Operador listarOperador(Integer id);
	
	public boolean editarOperador(Operador operador);
	
	public boolean excluirOperador(Operador operador);
	
}
