import { NumericItem } from '../../util/numeric-item.model';

export class PessoaEnum {

  static listaTipo = [
    new NumericItem('Pessoa Jurídica', 1),
    new NumericItem('Pessoa Física', 2),
  ];

  static CNPJ = 1;
  static CPF = 2;

}
