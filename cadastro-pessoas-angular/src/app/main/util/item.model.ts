export class Item {
    nome: string;
    codigo: string;

    constructor(nome: string, codigo: string) {
        this.nome = nome;
        this.codigo = codigo;
    }
}