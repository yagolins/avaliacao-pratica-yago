import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { OperadorModule } from './main/operador/operador.module';
import { PessoaModule } from './main/pessoa/pessoa.module';
import { LoginModule } from './login/login.module';
import { AdministradorGuard } from './main/util/guard/adm.guard';


const routes: Routes = [
  {
    path: 'operador',
    loadChildren: () => OperadorModule,
    canActivate: [AdministradorGuard],
    data: {
      breadcrumb: 'Operador'
    }
  },
  {
    path: 'login',
    loadChildren: () => LoginModule,
  },
  {
    path: 'pessoa',
    loadChildren: () => PessoaModule,
    data: {
      breadcrumb: 'Pessoa'
    }
  },
];

@NgModule({
  imports: [CommonModule,
    RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
