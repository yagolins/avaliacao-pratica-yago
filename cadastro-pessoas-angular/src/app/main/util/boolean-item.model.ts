export class BooleanItem {
    nome: string;
    value: boolean;

    constructor(nome: string, value: boolean) {
        this.nome = nome;
        this.value = value;
    }

    static simNao = [new BooleanItem('Sim', true), new BooleanItem('Não', false)]
}