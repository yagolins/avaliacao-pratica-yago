import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import {MatRadioModule} from '@angular/material/radio';
import { CadastrarPessoaComponent } from './cadastrar-pessoa/cadastrar-pessoa.component';

const routes = [
  {
    path: '',
    component: CadastrarPessoaComponent
  }
];

@NgModule({
  declarations: [CadastrarPessoaComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatRadioModule,
    RouterModule.forChild(routes)
  ],
  entryComponents: [CadastrarPessoaComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class PessoaModule { }
